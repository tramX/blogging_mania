from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from app_blogging_mania import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.IndexView.as_view(), name="index-page"),
    url(r'^subscribe/(\d{1,9})/$', views.subscribe, name='subscribe'),
    url(r'^create_post/$', login_required(views.CreatePost.as_view(), login_url='/admin/'), name='create_post'),
    url(r'^post_list/$', login_required(views.PostList.as_view(), login_url='/admin/'), name='post_list'),
    url(r'^read_post/(?P<pk>\d+)/$', login_required(views.ReadPost.as_view(), login_url='/admin/'), name='read_post'),
]
