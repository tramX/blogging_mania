from django.db import models
from django.conf import settings
from django.core.mail import EmailMessage


class Blog(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='author')
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='subscribers', blank=True)

    @property
    def user_list(self):
        return [user.id for user in self.users.all()]

    def __str__(self):
        return '{}'.format(self.user.username)


class PostBlog(models.Model):
    blog = models.ForeignKey(Blog, related_name='posts')
    title = models.CharField(max_length=255)
    text = models.TextField()
    user = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='readers', blank=True)
    date_of_publication = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    @property
    def user_list(self):
        return [user.id for user in self.user.all()]

    def save(self, *args, **kwargs):
        try:
            self.user
            send_message_status = False
        except:
            send_message_status = True
        result = super().save(*args, **kwargs)

        if send_message_status:
            subscribers = list(self.blog.users.values_list('email', flat=True))
            if len(subscribers) > 0:
                message = 'Alarm new post ' + "<a href='/read_post/" + str(self.id) + "/'>" + self.title + "</a>"
                send_email(subscribers, message)
        return result

    def delete(self, using=None, keep_parents=False):
        subscribers = list(self.blog.users.values_list('email', flat=True))
        if len(subscribers) > 0:
            message = 'Alarm del post ' + self.title
            send_email(subscribers, message)
        super().delete(using=None, keep_parents=False)

    def __str__(self):
        return '{} {}'.format(self.title, self.blog.user.username)


def send_email(emails, html, **kwargs):
    html = html.format(**kwargs)
    msg = EmailMessage('SUBJECT', html,
                       'admin@blogging.com', [emails])
    msg.content_subtype = "html"
    msg.send()
