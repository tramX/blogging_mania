from django.apps import AppConfig


class AppBloggingManiaConfig(AppConfig):
    name = 'app_blogging_mania'
