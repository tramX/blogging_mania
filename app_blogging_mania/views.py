from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy

from django.http import JsonResponse

from app_blogging_mania import models


class IndexView(ListView):
    model = models.Blog

    context_object_name = 'blogs'

    paginate_by = 3

    def get_template_names(self):
        return ['index.html', ]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.exclude(user_id=self.request.user.id)


@login_required
def subscribe(request, id):
    try:
        blog = models.Blog.objects.get(id=id)
        if request.user.id in list(blog.users.values_list('id', flat=True)):
            blog.users.remove(request.user)
            blog.save()

            for post in models.PostBlog.objects.filter(blog_id=blog.id):
                post.user.remove(request.user)
                post.save

            return JsonResponse({"result": True, "message": 'Подписаться'})
        else:
            blog.users.add(request.user)
            blog.save()

            return JsonResponse({"result": True, "message": 'Отписаться'})
    except:
        return JsonResponse({"result": False})


class CreatePost(CreateView):
    model = models.PostBlog

    fields = ['title', 'text']

    template_name = 'create_post.html'

    success_url = reverse_lazy('index-page')

    def get(self, request, *args, **kwargs):
        try:
            models.Blog.objects.get(user_id=self.request.user.id)
        except:
            models.Blog.objects.create(user_id=self.request.user.id)
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = self.object = form.save(commit=False)
        self.object.blog = models.Blog.objects.get(user_id=self.request.user.id)

        return super().form_valid(form)


class PostList(ListView):
    model = models.PostBlog

    context_object_name = 'posts'

    paginate_by = 3

    def get_template_names(self):
        return ['posts.html', ]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(blog_id__in=list(self.request.user.subscribers.values_list('id', flat=True)))


class ReadPost(DetailView):
    model = models.PostBlog

    def get_template_names(self):
        return ['post.html', ]

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)
        c['object'].user.add(self.request.user)
        c['object'].save()
        return c
