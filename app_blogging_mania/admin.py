from django.contrib import admin

from app_blogging_mania import models


admin.site.register(models.Blog)
admin.site.register(models.PostBlog)