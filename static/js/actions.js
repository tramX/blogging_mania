function subscribe(id) {
    var url = '/subscribe/' + id;
    var btnId = '#btn-' + id;

    $.get(url, function(data, status){
        $(btnId).text(data['message']);
    });
    return false;
}

function readPost(id) {
    var url = '/read_post/' + id;
    var btnId = '#btn-post-' + id;

    $.get(url, function(data, status){
        $(btnId).text('Прочитан');
    });
    return false;
}
